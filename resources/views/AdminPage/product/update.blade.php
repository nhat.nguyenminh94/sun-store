@extends('Adminpage.layouts.master')
@section('styles')
   <style type="text/css" media="screen">
        select option {
            color: #005f89;
        }

        label {
            text-transform: capitalize;
        }
    </style>
    <script src="/ckeditor/ckeditor.js"></script>
@endsection
@section('content')
 <div class="row">
        <div class="col-md-12">

            <div class="card ">
                <div class="card-header">
                    <h4 class="card-title">@if(isset($product)) Update Prodcut @else Create Product @endif</h4>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card-body">
                    <div class="table-responsive">
                        <form action="{{route('admin.product.update',['id'=>$product->id])}}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="Name Product">Name Product</label>
                                <input type="text" class="form-control" name="name" placeholder="Iphone 7 plus" value="{{old('name',isset($product->name) ? $product->name:'') }}">
                            </div>
                            <div class="form-group">
                                <label for="Color">Color</label>
                                <input type="text" class="form-control" name="color" placeholder="Rose Gold" value="{{old('color',isset($product->color) ? $product->color:'') }}">
                            </div>
                            <div class="form-group">
                                <label for="Categories">Categories</label>
                                <select class="form-control" id="" name="category_id">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}"{{isset($product) ? ($category->id == $product->category_id ? 'selected' : '') : '' }} >{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="price">price</label>
                                <input type="text" class="form-control" name="price" placeholder="VNĐ" value="{{old('price',isset($product->price) ? $product->price:'') }}">
                            </div>
                            <div class="form-group">
                                <label for="market price">market price</label>
                                <input type="text" class="form-control" name="market_price" placeholder="VNĐ" value="{{old('market_price',isset($product->market_price) ? $product->market_price:'') }}">
                            </div>
                            <div class="form-group">
                                <label for="quantity">quantity</label>
                                <input type="text" class="form-control" name="quantity" placeholder="Số lượng" value="{{old('quantity',isset($product->quantity) ? $product->quantity:'') }}">
                            </div>
                            <div class="form-group">
                                <label for="origin">origin</label>
                                <input type="text" class="form-control" name="origin" placeholder="Viet Nam" value="{{old('origin',isset($product->origin) ? $product->origin:'') }}">
                            </div>
                            <div class="form-group">
                                <label for="warranty">warranty</label>
                                <input type="number" class="form-control" name="warranty" placeholder="Số tháng" value="{{old('warranty',isset($product->warranty) ? $product->warranty:'') }}">
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 image_content">
                                    <div class="row">
                                        <button type="button" class="btn btn-primary btn_addimg" value="0">Thêm Hình
                                        </button>
                                    </div>
                                    <?php 
                                    // dd($product->images);
                                     ?>
                                    <div class="Add_img" data-key="">
                                     		@if($product->images[0])
										      @foreach($product->images as $key => $img)
                                        <div class="row item-image" style="border:2px solid;margin-bottom:10px;">
                                            <div class="col col-md-4">
                                                <label for="file-input" class=" form-control-label">File input</label> 
                                                <input type="file" id="file-input{{$key}}" name="image_upload filesTest" class="form-control-file upload-image" onchange="UploadImage(this);">
                                            </div>
                                            <div class="col col-md-6">
                                                <label for="file-input" class=" form-control-label">Image view</label><br>
                                                <span class="imageResult">
                                                    <img src="/{{isset($img['src']) ? $img['src'] : null}}" class="image_show" id="image_show{{$key}}" width="100px" height="100px">
                                                    <input type="hidden" name="image[{{$key}}][src]" value="{{$img['src']}}">
                                                    <input type="hidden" name="image[{{$key}}][id]" value="{{isset($img['id']) ? $img['id'] : null}}">
                                                </span>
                                            </div>
                                            <div class="col col-md-2" style="text-align: center;padding: 40px;">
                                                <button type="button" class="btn btn-danger" onclick="deleteItemimage(this);">Xóa</button>
                                            </div>
                                        </div>
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="detail">detail</label>
                                <textarea name="detail" id="detail" rows="10" cols="80" value="">
						{{old('detail',isset($product->detail) ? $product->detail:'') }}
						</textarea>
                                <script>
                                    // Replace the <textarea id="detail"> with a CKEditor
                                    // instance, using default configuration.
                                    CKEDITOR.replace('detail');
                                </script>
                            </div>
                            <fieldset class="form-group">
                                <div class="row">
                                    <legend class="col-form-label col-sm-2 pt-0">Status</legend>
                                    <div class="col-sm-10">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="status" id="gridRadios1"
                                                   value="1" checked>
                                            <label class="form-check-label" for="gridRadios1">
                                                Active
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="status" id="gridRadios2"
                                                   value="2">
                                            <label class="form-check-label" for="gridRadios2">
                                                Not Active
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="col-auto">
                                <button type="submit" class="btn btn-primary mb-2">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
         var countImage = $('.Add_img').find('.item-image').length + 1;
    	   $('.btn_addimg').click(function () {
           
            console.log(countImage);
            var src = 'src';
            if (countImage > 6) {
                alert("Không được quá 6 tấm hình");
            } else {
                $('.Add_img').append(
                    '<div class="row item-image" style="border:2px solid;margin-bottom:10px;">'
                    + '<div class="col col-md-4">'
                    + '<label for="file-input" class=" form-control-label">File input</label> '
                    + '<input type="file" id="file-input' + countImage + '" name="image_upload filesTest" class="form-control-file upload-image" onchange="UploadImage(this);">'
                    + '</div>'
                    + '<div class="col col-md-6">'
                    + '<label for="file-input" class=" form-control-label">Image view</label><br>'
                    + '<span class="imageResult">'
                    + '<img src="/uploads/upload.png" class="image_show" id="image_show' + countImage + '" width="100px" height="100px">'
                    + '<input type="hidden" name="image[' + countImage +'][src]" id="images_show_data' + countImage + '" value="">'
                     + '<input type="hidden" name="image[' + countImage +'][id]" value="">'
                    + '</span>'
                    + '</div>'
                    + '<div class="col col-md-2" style="text-align: center;padding: 40px;">'
                    + '<button type="button" class="btn btn-danger" onclick="deleteItemimage(this);">Xóa</button>'
                    + '</div>'
                    + '</div>'
                )
            }
        });

    	function UploadImage(e) {
            var a = $(e).parent('div').parent('div').find('span.imageResult');
            var b = $(a).children('img').attr('id');
            var c = $(a).children('input').attr('id');
            var b2 = '#' + b;
            var c2 = '#' + c;
            console.log(e.files[0]);
            var form_data = new FormData();
            form_data.append('file', e.files[0]);
            form_data.append('_token', '{{csrf_token()}}');
            $.ajax({
                url: "{{route('admin.product.uploadimage',['type'=>'products'])}}", // Url to which the request is send
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                success: function (res)   // A function to be called if request succeeds
                {
                    $(b2).attr('src', '');
                    $(c2).val('');
                    dataSuccess = JSON.parse(res);
                    $(b2).attr('src', '/' + dataSuccess.filename);
                    $(c2).val(dataSuccess.filename);
                }, error: function (error) {
                    alert(error);
                }
            });
        };   
        // function deleteItemimage(e) {
        //     console.log(e);
        //     var a = $(e).parent('div').parent('div').find('span.imageResult');
        //     var srcImg = $(a).children('img').attr('src');
        //     var data_Img = new FormData();
        //     data_Img.append('srcImg', srcImg);
        //     data_Img.append('_token', '{{csrf_token()}}');
        //     $.ajax({
        //         url: "{{route('admin.product.deleteimage')}}",
        //         type: 'POST',
        //         data: data_Img,
        //         contentType: false,
        //         processData: false,
        //         success: function (res)   // A function to be called if request succeeds
        //         {
        //             var d =  $(e).parent();
        //             $(d).parent('.item-image').remove();
        //             var num = +$(".btn_addimg").val() - 1;
        //             $(".btn_addimg").val(num);
        //         }, error: function (error) {
        //             alert(error);
        //         }
        //     });
        // };
          function deleteItemimage(e) {
            var a = $(e).parent('div').parent('div').find('span.imageResult');
            var d =  $(e).parent();
            $(d).parent('.item-image').remove();
            var num = $(".btn_addimg").val() - 1;
           //  var addimg = $('.Add_img').data('key');
           // var test =  $('.Add_img').attr('data-key',addimg - 1);
           //  console.log(test.data('key'));
            $(".btn_addimg").val(num);
        };
    </script>
@endsection