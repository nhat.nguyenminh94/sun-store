@extends('Adminpage.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12">

		<div class="card ">
			<div class="card-header">
				<h4 class="card-title"> Products Table</h4>
			</div>
			<div class="row">
			<div class="col-md-6">
				<a class="btn btn-success text-center" href="{{route('admin.product.add')}}" style="width: 100%;margin-bottom: 10px;">
				Add </a>
			</div>         
		</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table tablesorter " id="">
						<thead class=" text-primary">
							<tr>
								<th>
									ID
								</th>
								<th>
									Name
								</th>
								<th>
									Color
								</th>
								<th class="text-center">
									Price
								</th>
								<th class="text-center">
									Market Price
								</th>
								<th class="text-center">
									Quantity
								</th>
								<th class="text-center">
									Category
								</th>
								<th class="text-center">
									Status
								</th>
							</tr>
						</thead>
						<tbody>
						@if($products)
							@foreach($products as $product)
								<tr>
									<td>
										{{$product->id}}
									</td>
									<td>
										{{$product->name}}
									</td>
									<td>
										{{$product->color}}
									</td>
									<td class="text-center">
										{{$product->price}}
									</td>
									<td class="text-center">
										{{$product->market_price}}
									</td>
									<td class="text-center">
										{{$product->quantity}}
									</td>
									<td class="text-center">
										{{$product->category->name}}
									</td>
									<td>
										{{$product->status}}
									</td>
									<td>
										<a class="btn btn-primary" href="{{route('admin.product.edit',['id'=>$product->id])}}">Update</a>
									</td>
									<td>
										<form action="{{route('admin.product.destroy',['id'=>$product->id])}}" method="POST">
										@method('DELETE')
										@csrf
										<button class="btn btn-danger">Delete</button>
										</form>    
									</td>
								</tr>
							@endforeach
						@endif

						</tbody>
					</table>
				</div>
			</div>
			<div class="row" style="margin:0 auto;">
				{{$products->links()}}
			</div>
		</div>
	</div>
</div>
@endsection