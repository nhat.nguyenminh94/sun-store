@extends('Adminpage.layouts.master')
@section('content')
<div class="row">
	<div class="col-md-12">

		<div class="card ">
			<div class="card-header">
				<h4 class="card-title"> Category Table</h4>
			</div>
			<div class="row">
			<div class="col-md-6">
				<a class="btn btn-success text-center" href="{{route('admin.category.add')}}" style="width: 100%;margin-bottom: 10px;">
				Add </a>
			</div>         
		</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table tablesorter " id="">
						<thead class=" text-primary">
							<tr>
								<th>
									ID
								</th>
								<th>
									Name
								</th>
								<th>
									Status
								</th>
								<th class="text-center">
									Parent ID
								</th>
								<th class="text-center">
									Parent Name
								</th>
							</tr>
						</thead>
						<tbody>
						@if($categories)
							@foreach($categories as $category)
								<tr>
									<td>
										{{$category->id}}
									</td>
									<td>
										{{$category->name}}
									</td>
									<td>
										{{$category->status}}
									</td>
									<td class="text-center">
										{{$category->parent_id}}
									</td>
									<td class="text-center">
										{{$category->parent->name ?? "Error Or Parent"}}
									</td>
									<td>
										<a class="btn btn-primary" href="{{route('admin.category.edit',['id'=>$category->id])}}">Update</a>
									</td>
									<td>
										<form action="{{route('admin.category.destroy',['id'=>$category->id])}}" method="POST">
										@method('DELETE')
										@csrf
										<button class="btn btn-danger">Delete</button>
										</form>    
									</td>
								</tr>
							@endforeach
						@endif

						</tbody>
					</table>
				</div>
			</div>
			<div class="row" style="margin:0 auto;">
				{{$categories->links()}}
			</div>
		</div>
	</div>
</div>
@endsection