<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['prefix' => 'admin', 'namespace' => 'Admin','as'=>'admin.'], function () {
    Route::get('/', 'AdminController@index')->name('index');
    Route::group(['prefix' => 'category','as'=>'category.'], function () {
        Route::get('/', 'CategoryController@index')->name('list');
        Route::get('/add', 'CategoryController@create')->name('add');
        Route::post('/created', 'CategoryController@store')->name('created');
        Route::get('edit/{id}', 'CategoryController@edit')->name('edit');
        Route::put('/{id}', 'CategoryController@update')->name('update');
        Route::delete('/category/{id}', 'CategoryController@destroy')->name('destroy');
    });
    Route::group(['prefix' => 'product','as'=>'product.'], function () {
        Route::get('/', 'ProductController@index')->name('list');
        Route::get('/add', 'ProductController@create')->name('add');
        Route::post('/created', 'ProductController@store')->name('created');
        Route::get('edit/{id}', 'ProductController@edit')->name('edit');
        Route::post('/{id}', 'ProductController@update')->name('update');
        Route::delete('/product/{id}', 'ProductController@destroy')->name('destroy');
         Route::post('uploadimage/{type?}', 'ProductController@uploadImage')->name('uploadimage');
           Route::post('/deleteimage', 'ProductController@deleteImage')->name('deleteimage');
    });
});
