<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Product::class, function (Faker $faker) {
	$name = $faker->name;
    return [
        'name' => $name,
        'color' => $faker->colorName,
        'price' => $faker->randomNumber(3),
        'market_price' => $faker->randomNumber(2),
        'quantity' => $faker->randomNumber(1),
        'detail' => $faker->text,
        'origin' => $faker->country,
        'warranty' => rand(0,24),
        'user_id' => rand(1,5),
        'category_id' => App\Models\Category::all()->random()->id,
        'status' => rand(1,2),
        'view' => 1,
    ];
});
