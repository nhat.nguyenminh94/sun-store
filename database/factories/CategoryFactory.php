<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Category::class, function (Faker $faker) {
	$name = $faker->name;
    return [
        'name' => $name,
        'slug' => str_slug($name),      
        'status' => rand(1,2),
        'parent_id' => rand(0,10),
    ];
});
