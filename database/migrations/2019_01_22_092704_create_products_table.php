<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('color')->nullable();
            $table->integer('price');
            $table->integer('market_price')->nullable();
            $table->integer('quantity');
            $table->longText('detail');
            $table->string('origin');
            $table->Integer('warranty');
            $table->Integer('user_id');
            $table->Integer('category_id');
            $table->Integer('comment_id')->nullable();
            $table->tinyInteger('status');
            $table->Integer('view')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
