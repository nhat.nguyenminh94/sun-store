<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Image;
use File;
use App\Http\Requests\ProductRequest;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $categories;

    public function __construct()
    {
        $this->categories = Category::select()->get();
    }

    public function index()
    {

        $products = Product::with('category')->select()->paginate(5);
        return view('AdminPage.product.list',
            [
                'products' => $products
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = $this->categories;
        return view('AdminPage.product.createOrUpdate', compact('categories'));
    }


    public function handleStoreOrUpdate($request, $id = null)
    {
        $dataInput = array_merge($request->all(), [
            'user_id' => 1,
        ]);
        $product = Product::create($dataInput);
        foreach ($request->image as $val) {
            $dataImage['src'] = $val['src'];
            $dataImage['alt'] = "img";
            $dataImage['description'] = "test";
            $product->images()->create($dataImage);
          
        }
        return $product;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $this->handleStoreOrUpdate($request);
        return redirect()->route('admin.product.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $categories = $this->categories;
        $product = Product::findOrFail($id);
        return view('AdminPage.product.update',['product'=> $product,
        'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $images_request = $request->image;
        dd($images_request);
        $product = Product::findOrFail($id);
        $images_data = $product->images()->pluck('id')->toArray();
        foreach($images_request as $value){
            // dd($value['id']);
            if(empty($value['id']) && !in_array($value['id'], $images_data)){
                // $product->images()->create();
                echo "tao moi ";
                echo "<br>";
            }else{
                echo "update";
                 echo "<br>";
                 $pos = array_search($value, $images_data); 
                 unset($images_data[$pos]); 
            }
        }
        dd($images_data);
        if(sizeof($images_data) > 0 ){
            $listImageDel = Image::whereIn('id',$images_data)->get();
                echo "xoa";
                 echo "<br>";
                // foreach ($listImageDel as $key => $value) {
                //     $value->delete();

                // }
        }
        exit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Product::findOrFail($id);
        $products->delete();
        return redirect()->route('admin.product.list');
    }

    public function uploadImage($type = null, Request $request)
    {
        $extension = $request->file('file')->getClientOriginalExtension();
        if ($type) {
            $dir = 'uploads/' . $type . '/';
        } else {
            $dir = 'uploads/';
        }
        $filename = uniqid() . '_' . time() . '.' . $extension;
        $request->file('file')->move($dir, $filename);
        $nameAndPatch = $dir . $filename;
        return json_encode(['status' => true, 'filename' => $nameAndPatch]);
    }

    public function deleteImage(Request $request)
    {
        // $srcImg = $request->srcImg;
        // $srcImg = public_path() . $srcImg;
        // if ($srcImg != public_path() . '/uploads/upload.png') {
        //     File::delete($srcImg);
            // return json_encode(['status' => true,]);
        // }
    }
}
