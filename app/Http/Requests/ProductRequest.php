<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required',
            'color' => 'required',
            'price'     => 'required|numeric',
            'quantity'     => 'required|numeric',
            'detail'     => 'required',
            'origin'     => 'required',
            'warranty'     => 'required|numeric',
            'image' => 'present',
            'file' => 'image',
        ];
    }
    public function messages()
    {
        return [
            'name.required'       => 'Tên Product không được để trống',
//            'name.unique'       => 'Tên Product không được trùng',
            'color.required' => 'Color không được để trống',
            'price.required'     => 'Giá không được để trống',
            'price.numeric'     => 'Giá phải là số',
            'quantity.numeric'     => 'số lượng phải là số',
            'quantity.required'     => 'số lượng không được để trống',
            'detail.required'     => 'chi tiết không được để trống',
            'origin.required'     => 'xuất xứ không được để trống',
            'warranty.required'     => 'bảo hành không được để trống',
            'warranty.numeric'     => 'bảo hành phải là số',
            'image.present' => 'Bài viết phải có ít nhất một hình',
            'file.image' => 'The file must be an image (jpeg, png, bmp, gif, or svg)',
        ];
    }
}
