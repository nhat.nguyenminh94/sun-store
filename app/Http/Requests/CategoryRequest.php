<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|unique',
            'slug' => 'required',
            'status'     => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required'       => 'Tên Category không được để trống',
            'name.unique'       => 'Tên Category không được trùng',
            'slug.required' => 'Slug không được để trống',
            'status.required'     => 'Status không được để trống',
        ];
    }
}
