<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
   protected $fillable=['id','name','color','price','market_price','quantity','detail','origin','warranty','user_id','category_id','comment_id','img_pd_id','status','view'];

   public function category()
   {
   		return $this->belongsTo('App\Models\Category','category_id');
   }

    public function images()
    {
        return $this->morphMany(Image::class, 'imagetable');
    }
}
